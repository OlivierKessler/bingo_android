package com.epsi.bingo.Preferences;

import android.app.Activity;
import android.content.SharedPreferences;



public class PreferencesBingo {
    SharedPreferences prefs;

    public PreferencesBingo(Activity activity){
        prefs = activity.getSharedPreferences("prefs", 0);
    }

    public String getName(){
        return prefs.getString("name", "Olivier");
    }

    public boolean getMusicState(){
        return prefs.getBoolean("music", true);
    }

    public String getAvatarPath(){
        return prefs.getString("avatarpath", "");
    }

    public void setName(String name){
        prefs.edit().putString("name", name).commit();
    }

    public void setMusicState(Boolean enable){
        prefs.edit().putBoolean("music", enable).commit();
    }

    public void setAvatar(String path){
        prefs.edit().putString("avatarpath",path).commit();
    }
}



