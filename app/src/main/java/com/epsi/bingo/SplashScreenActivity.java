package com.epsi.bingo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.HashMap;

import com.epsi.bingo.Data.DatabaseHandler;
import com.epsi.bingo.Interfaces.INameChanger;
import com.epsi.bingo.Preferences.PreferencesBingo;

import org.json.JSONException;
import org.json.JSONObject;


public class SplashScreenActivity extends Activity implements INameChanger {

    Button btnGame;
    Button btnRules;
    ListView listBingos;
    SimpleAdapter simpleAdapter;
    String idCurrentUser;
    ArrayList<HashMap<String, String>> bingosList;

    public final static String TAG_USER_ID = "CURRENT_USER_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        bingosList = new ArrayList<>();
        btnGame    = (Button)findViewById(R.id.button_jouer);
        btnRules   = (Button)findViewById(R.id.button_rules);
        listBingos = (ListView)findViewById(R.id.list_bingos);

        this.showDialogChangeName();
        this.populateListView();

        btnGame.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                // Launch Dashboard Screen
                Intent bingoScreen = new Intent(getApplicationContext(), BingoActivity.class);

                // Close all views before launching Dashboard
                bingoScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                bingoScreen.putExtra(TAG_USER_ID, idCurrentUser);
                startActivity(bingoScreen);
            }
        });


        btnRules.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                // Launch Dashboard Screen
                Intent rulesScreen = new Intent(getApplicationContext(), RulesActivity.class);

                // Close all views before launching Dashboard
                rulesScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                rulesScreen.putExtra(TAG_USER_ID, idCurrentUser);
                startActivity(rulesScreen);
            }
        });
    }

    public void populateListView(){
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        String[][] bingos  = db.getAllBingos();

        if (bingos != null && bingos[0][0] != null) {
            // Parties found
            // Looping through All Parties
            Log.d("Nombre de parties", Integer.toString(bingos.length));
            for (int i = 0; i < bingos.length; i++) {
                String[] currentBingo = bingos[i];

                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();

                // adding each child node to HashMap key => value
                map.put(DatabaseHandler.getKeyIdBingo(),   getString(R.string.id_partie)   + currentBingo[0]);
                map.put(DatabaseHandler.getKeyDateBingo(), getString(R.string.date_partie) + currentBingo[1]);
                map.put(DatabaseHandler.getKeyNbShots(),   getString(R.string.nb_shots)    + currentBingo[2]);
                map.put(DatabaseHandler.getKeyPseudoUser(),getString(R.string.username)    + currentBingo[3]);

                // adding HashList to ArrayList
                bingosList.add(map);
            }
        }

        /**
         * Set the Simple Adapter to the listView, item.xml representing an item of the list
         */
        this.simpleAdapter = new SimpleAdapter(
                this,
                bingosList,
                R.layout.item,
                new String[] {
                        DatabaseHandler.getKeyIdBingo(),
                        DatabaseHandler.getKeyDateBingo(),
                        DatabaseHandler.getKeyNbShots(),
                        DatabaseHandler.getKeyPseudoUser()
                },
                new int[]{
                        R.id.id_bingo,
                        R.id.date_bingo,
                        R.id.finished_bingo,
                        R.id.id_user
                }
        );

        listBingos.setAdapter(simpleAdapter);
    }


    @Override
    public void changeName(String name){
        /**
         * Create a new user
         */
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        try {
            this.idCurrentUser = db.addUserIfNotExists(
                    new JSONObject().put(DatabaseHandler.KEY_PSEUDO_USER, name)
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new PreferencesBingo(this).setName(name);
    }

    @Override
    public void showDialogChangeName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose pseudo");

        final EditText input        = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(new PreferencesBingo(this).getName());

        builder.setView(input);
        builder.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                changeName(input.getText().toString());
            }
        });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
