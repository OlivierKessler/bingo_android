package com.epsi.bingo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.epsi.bingo.Data.DatabaseHandler;
import com.epsi.bingo.Interfaces.INameChanger;
import com.epsi.bingo.Preferences.PreferencesBingo;

import org.json.JSONException;
import org.json.JSONObject;




public class BingoActivity extends ActionBarActivity implements INameChanger {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bingo);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new BingoFragmentActivity())
                    .commit();
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        setUserIdToFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.change_name) {
            showDialogChangeName();
        }else if (id == R.id.change_avatar){
            showDialogChangeAvatar();
        }else if(id == R.id.change_music_state){
            showDialogChangeMusicState();
        }

        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case 1234:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    changeAvatar(filePath);
                }
        }

    };

    public void setUserIdToFragment(){
        Intent i = getIntent();
        BingoFragmentActivity wf = (BingoFragmentActivity)getSupportFragmentManager()
                .findFragmentById(R.id.container);
        String ss = i.getStringExtra(SplashScreenActivity.TAG_USER_ID);
        wf.setUserId(ss);
    }

    @Override
    public void changeName(String name){
        /**
         * Create a new user
         */
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        try {
            db.addUserIfNotExists(new JSONObject().put(DatabaseHandler.KEY_PSEUDO_USER, name));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        BingoFragmentActivity wf = (BingoFragmentActivity)getSupportFragmentManager()
                .findFragmentById(R.id.container);
        wf.changeName(name);
        new PreferencesBingo(this).setName(name);
    }

    public void changeAvatar(String avatarPath){
        Bitmap yourSelectedImage = BitmapFactory.decodeFile(avatarPath);

        BingoFragmentActivity wf = (BingoFragmentActivity)getSupportFragmentManager()
                .findFragmentById(R.id.container);
        wf.changeAvatar(yourSelectedImage);
        new PreferencesBingo(this).setAvatar(avatarPath);
    }

    public void changeMusicState(Boolean musicState){
        BingoFragmentActivity wf = (BingoFragmentActivity)getSupportFragmentManager()
                .findFragmentById(R.id.container);
        wf.changeMusicState(musicState);
        new PreferencesBingo(this).setMusicState(musicState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bingo, menu);
        return true;
    }


    private void showDialogChangeMusicState(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enable/Disable Music");
        final Switch switch1 = new Switch(this);
        switch1.setChecked(new PreferencesBingo(this).getMusicState());

        builder.setView(switch1);
        builder.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                changeMusicState(switch1.isChecked());
            }
        });
        builder.show();
    }

    @Override
    public void showDialogChangeName(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Change pseudo");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(new PreferencesBingo(this).getName());

        builder.setView(input);
        builder.setPositiveButton("Go", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                changeName(input.getText().toString());
            }
        });
        builder.show();
    }


    private void showDialogChangeAvatar(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Change Avatar");
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                final int ACTIVITY_SELECT_IMAGE = 1234;
                startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
            }
        });
        builder.show();
    }
}
