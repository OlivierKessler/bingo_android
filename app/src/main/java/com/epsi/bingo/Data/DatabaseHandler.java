package com.epsi.bingo.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseHandler extends SQLiteOpenHelper {

    /**
     * Settings
     */
    private static final int DATABASE_VERSION       = 3;
    private static final String DATABASE_NAME       = "bingo";

    /**
     * Tables Names
     */
    private static final String TABLE_BINGO_HISTORY = "bingo_history";
    private static final String TABLE_USER          = "users";
    /**
     * Fields Names
     */
    public static final String KEY_ID_BINGO         = "id_bingo";
    public static final String KEY_DATE_BINGO       = "date_start";
    public static final String KEY_NB_SHOTS         = "nb_shots";

    public static final String KEY_ID_USER          = "id_user";
    public static final String KEY_PSEUDO_USER      = "pseudo";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static String getKeyIdBingo(){
        return KEY_ID_BINGO;
    }

    public static String getKeyDateBingo(){
        return KEY_DATE_BINGO;
    }

    public static String getKeyNbShots(){
        return KEY_NB_SHOTS;
    }

    public static String getKeyIdUser(){
        return KEY_ID_USER;
    }

    public static String getKeyPseudoUser(){
        return KEY_PSEUDO_USER;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_BINGO_HISTORY = "CREATE TABLE " + TABLE_BINGO_HISTORY + "("
                + KEY_ID_BINGO    + " INTEGER PRIMARY KEY,"
                + KEY_DATE_BINGO  + " DATE,"
                + KEY_NB_SHOTS    + " INTEGER,"
                + KEY_ID_USER     + " INTEGER, "
                + " FOREIGN KEY ("+ KEY_ID_USER +") REFERENCES "+TABLE_USER+" ("+KEY_ID_USER+"));";

        String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID_USER     + " INTEGER PRIMARY KEY,"
                + KEY_PSEUDO_USER + " TEXT)";

        db.execSQL(CREATE_TABLE_USER);
        Log.e("SQLite", "Table Users crée");

        db.execSQL(CREATE_TABLE_BINGO_HISTORY);
        Log.e("SQLite", "Table Bingo crée");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BINGO_HISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }

    /**
     *
     * Bingo Operations
     *
     */
    public String addBingo(JSONObject jsonObject) {
        SQLiteDatabase db    = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            values.put(KEY_DATE_BINGO, jsonObject.getString(KEY_DATE_BINGO));
            values.put(KEY_ID_USER,    jsonObject.getInt(KEY_ID_USER));
            values.put(KEY_NB_SHOTS,   jsonObject.getString(KEY_NB_SHOTS));


            db.insert(TABLE_BINGO_HISTORY, null, values);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String selectInsertedUser = "SELECT last_insert_rowid() as id FROM " + TABLE_BINGO_HISTORY;
        Cursor cursor             = db.rawQuery(selectInsertedUser, null);

        if (cursor.moveToFirst()) {
            String val =  cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();
            db.close();
            return val;
        }
        db.close();
        return null;
    }

    public String[][] getAllBingos(){
        SQLiteDatabase db   = this.getReadableDatabase();
        String selectBingo  = "SELECT  * FROM " + TABLE_BINGO_HISTORY
                + " INNER JOIN " + TABLE_USER + " ON " + TABLE_BINGO_HISTORY + "." + getKeyIdUser()
                + " = " + TABLE_USER + "." + getKeyIdUser();
        Cursor cursor       = db.rawQuery(selectBingo, null);

        if (cursor.moveToFirst()) {
            int count       = cursor.getCount();
            String[][] vals = new String[count][4];

            for(int i = 0; i < count; i++){
                vals[i]     = new String[4];
                vals[i][0]  = cursor.getString(cursor.getColumnIndex(KEY_ID_BINGO));
                vals[i][1]  = cursor.getString(cursor.getColumnIndex(KEY_DATE_BINGO));
                vals[i][2]  = cursor.getString(cursor.getColumnIndex(KEY_NB_SHOTS));
                vals[i][3]  = cursor.getString(cursor.getColumnIndex(KEY_PSEUDO_USER));
                cursor.moveToNext();
            }
            cursor.close();
            db.close();
            return vals;
        }
        db.close();
        return null;
    }

    /**
     *
     * Users Operations
     *
     */
    public String addUser(JSONObject jsonObject) {
        SQLiteDatabase db    = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        try {
            values.put(KEY_PSEUDO_USER, (String) jsonObject.getString(KEY_PSEUDO_USER));
            db.insert(TABLE_USER, null, values);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        String selectInsertedUser = "SELECT last_insert_rowid() as id FROM " + TABLE_USER;
        Cursor cursor             = db.rawQuery(selectInsertedUser, null);

        if (cursor.moveToFirst()) {
            String val =  cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();
            db.close();
            return val;
        }
        db.close();
        return null;
    }

    public String getIdUserByName(JSONObject jsonObject){
        SQLiteDatabase db   = this.getWritableDatabase();
        String selectIdUser = null;

        try {
            selectIdUser = "SELECT "+KEY_ID_USER+" as id FROM " + TABLE_USER + " WHERE "
                    + KEY_PSEUDO_USER + " = \"" + jsonObject.getString(KEY_PSEUDO_USER)+"\"";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Cursor cursor = db.rawQuery(selectIdUser, null);

        if (cursor.moveToFirst()) {
            String val =  cursor.getString(cursor.getColumnIndex("id"));
            cursor.close();
            db.close();
            return val;
        }
        db.close();
        return null;
    }

    public String addUserIfNotExists(JSONObject jsonObject){
        String idUserFromPseudo = this.getIdUserByName(jsonObject);
        if(idUserFromPseudo != null){
            return idUserFromPseudo;
        }
        else{
            return this.addUser(jsonObject);
        }
    }
}
