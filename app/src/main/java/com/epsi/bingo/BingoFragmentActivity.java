package com.epsi.bingo;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.epsi.bingo.Data.DatabaseHandler;
import com.epsi.bingo.Preferences.PreferencesBingo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class BingoFragmentActivity extends Fragment {
    protected GridView gridViewBingo;
    protected Button launcherButton;
    protected String musicSource;
    protected String idCurrentUser;
    protected int nbShots;

    private List<Integer> possibleNumbersToDrawn = new ArrayList<>();
    protected final static int MIN_BINGO = 1;
    protected final static int MAX_BINGO = 75;

    ViewGroup container;
    protected TextView choosenNumber;
    protected TextView pseudo;

    MediaPlayer mp = new MediaPlayer();


    protected final static int nbLineGrid = 5;
    protected final static int nbColumnsGrid = 5;

    List<Integer> gridNumbers  = new ArrayList<>(25);
    Boolean[][] checkedNumbers = new Boolean[][]{
            new Boolean[]{false, false, false, false, false},
            new Boolean[]{false, false, false, false, false},
            new Boolean[]{false, false, false, false, false},
            new Boolean[]{false, false, false, false, false},
            new Boolean[]{false, false, false, false, false}
    };

    /**
     * This Lists contains randomly sorted elements between ranges,
     * populateNumbers()  get the 5th firsts of each to make the GridView data row
     */
    List<Integer> random115;
    List<Integer> random1630;
    List<Integer> random3145;
    List<Integer> random4660;
    List<Integer> random6175;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.initializeRandomized();
        this.populateFinalDataRow();
        this.nbShots = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_bingo, container, false);

        gridViewBingo  = (GridView)rootView.findViewById(R.id.gridViewParties);
        launcherButton = (Button)rootView.findViewById(R.id.launcherButton);
        choosenNumber  = (TextView)rootView.findViewById(R.id.randomNumberTextView);
        pseudo         = (TextView)rootView.findViewById(R.id.pseudo);

        try {
            this.musicSource = "android.resource://com.epsi.bingo/raw/s_m_b";

            mp.setDataSource(rootView.getContext(), Uri.parse(this.musicSource));
            mp.setLooping(true);
            mp.prepare();
            mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        }


        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(rootView.getContext(),
                android.R.layout.simple_list_item_1, this.gridNumbers);

        gridViewBingo.setAdapter(adapter);


        gridViewBingo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                int colIndex    = position%nbLineGrid;
                int lineIndex   = (position-(colIndex))/nbLineGrid;

                Log.d("coolll" , Integer.toString(colIndex));
                Log.d("ligne" , Integer.toString(lineIndex));
                int valueOfCase = Integer.parseInt(((TextView)v).getText().toString());

                if(choosenNumber.getText().toString().equals("")){
                    Toast.makeText(rootView.getContext(),
                            "You have to generate a number before playing", Toast.LENGTH_SHORT).show();
                    return;
                }

                /**
                 * If the user click on a number that is not the drawn one
                 */

                if(valueOfCase != Integer.parseInt(choosenNumber.getText().toString())) {
                    Toast.makeText(rootView.getContext(),
                            "The number you clicked on is not the drawn number", Toast.LENGTH_SHORT).show();
                    return;
                }

                /**
                 Check if this is the first time we click on the item
                 */
                if (checkedNumbers[lineIndex][colIndex])
                    checkedNumbers[lineIndex][colIndex] = false;
                else
                    checkedNumbers[lineIndex][colIndex] = true;

                /**
                 * Color clicked item
                 */
                v.setBackgroundColor(0xFFFA5757);

                /**
                 * If the game is over, insert game datas in Database
                 */
                if(checkForEnding(lineIndex, colIndex)){
                    Toast.makeText(rootView.getContext(),
                            "YOU WON", Toast.LENGTH_LONG).show();

                    DatabaseHandler db = new DatabaseHandler(rootView.getContext());
                    JSONObject bingo   = new JSONObject();
                    try {
                        bingo.put(db.getKeyIdUser(), idCurrentUser);
                        bingo.put(db.getKeyDateBingo(), new Date().toString());
                        bingo.put(db.getKeyNbShots(), nbShots);

                        db.addBingo(bingo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        launcherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosenNumber.setText(Integer.toString(generateDrawnNumber()));
                nbShots++;
            }
        });

        this.container = container;

        PreferencesBingo prefs = new PreferencesBingo(getActivity());
        String name = prefs.getName();
        this.changeName(prefs.getName());
        String path = prefs.getAvatarPath();
        if(!path.equals("")) {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            this.changeAvatar(bitmap);
        }
        this.changeMusicState(prefs.getMusicState());


        return rootView;
    }



    /**
     * @param rangeLow
     * @param rangeHigh
     * @return randomly sorted elements from the provided range
     */
    public List<Integer> generateNumbers(int rangeLow, int rangeHigh){
        List<Integer> mixedNb = new ArrayList<>();

        for (int i = rangeLow; i <= rangeHigh; i++)
            mixedNb.add(i);
        Collections.shuffle(mixedNb);
        mixedNb = mixedNb.subList(0, 5);
        Collections.sort(mixedNb);

        return mixedNb;
    }

    public int generateDrawnNumber(){
        int drawnNumber;

        if(this.possibleNumbersToDrawn.isEmpty()) {
            for (int i = this.MIN_BINGO; i <= this.MAX_BINGO; i++) {
                this.possibleNumbersToDrawn.add(i);
            }
            Collections.shuffle(this.possibleNumbersToDrawn);
        }

        drawnNumber = this.possibleNumbersToDrawn.get(0);
        this.possibleNumbersToDrawn.remove(0);

        return drawnNumber;
    }

    /**
     * Set the final data Row to display
     */
    public void populateFinalDataRow(){
        int j;

        for(j = 0; j< 5; j++) {
            this.gridNumbers.add(this.random115.get(j));
            this.gridNumbers.add(this.random1630.get(j));
            this.gridNumbers.add(this.random3145.get(j));
            this.gridNumbers.add(this.random4660.get(j));
            this.gridNumbers.add(this.random6175.get(j));
        }
    }

    public void initializeRandomized(){
        this.random115  = this.generateNumbers(1, 15);
        this.random1630 = this.generateNumbers(16, 30);
        this.random3145 = this.generateNumbers(31, 45);
        this.random4660 = this.generateNumbers(46, 60);
        this.random6175 = this.generateNumbers(61, 75);
    }

    public boolean checkForEnding(int lineOfClicked, int columnOfClicked)
    {
        boolean linePresence  = false;
        boolean colPresence   = false;
        boolean diag1Presence = false;
        boolean diag2Presence = false;

        /**
         * Check for linePresence
         */
        for(int i = 0; i < nbColumnsGrid; i++) {
            if (checkedNumbers[lineOfClicked][i])
                linePresence = true;
            else
            {
                linePresence = false;
                break;
            }
        }

        /**
         * Check for colPresence
         */
        for(int j = 0; j < nbLineGrid; j++) {
            if (checkedNumbers[j][columnOfClicked])
                colPresence = true;
            else
            {
                colPresence = false;
                break;
            }
        }

        /**
         * Check for the first diagonal
         */
        for(int k = 0; k < nbLineGrid; k++){
            if(checkedNumbers[k][k])
                diag1Presence = true;
            else
            {
                diag1Presence = false;
                break;
            }
        }

        /**
         * Check for the second diagonal
         */
        int g = nbColumnsGrid - 1;
        for(int l = 0; l < nbLineGrid; l++){
            if(checkedNumbers[l][g])
                diag2Presence = true;
            else {
                diag2Presence = false;
                break;
            }
            g--;
        }

        return linePresence || colPresence || diag1Presence || diag2Presence;
    }


    public void changeName(String name){
        try {
            pseudo.setText(getString(R.string.hello) + " , " + name);

        }catch(Exception e){
            Log.e("Bingo", "Error while updating pseudo");
        }
    }

    public void changeAvatar(Bitmap image){
        this.container.setBackground(new BitmapDrawable(getResources(), image));
    }

    public void changeMusicState(Boolean state){
        Log.d("playing", Boolean.toString(state && !mp.isPlaying()));
        if(state && !mp.isPlaying()){
            mp.start();
        }
        else if(state && mp.isPlaying()){
            Log.d("coucou", "coucou");
        }
        else{
            mp.pause();
        }
    }

    public void setUserId(String id){
        idCurrentUser = id;
    }
}
