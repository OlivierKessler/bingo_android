package com.epsi.bingo.Interfaces;

/**
 * Created by olivier on 21/04/2015.
 */
public interface INameChanger {

    public void changeName(String name);
    public void showDialogChangeName();
}
